<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 30.08.17
 * Time: 20:49
 */

namespace App\Inspections;


interface InspectionInterface
{
    public function detect($body);
}