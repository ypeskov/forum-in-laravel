<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 30.08.17
 * Time: 20:47
 */

namespace App\Inspections;

use Exception;


class InvalidKeywords implements InspectionInterface
{
    protected $keywords = [
        'yahoo customer support',
    ];


    public function detect($body)
    {
        foreach ($this->keywords as $keyword) {
            if (stripos($body, $keyword) !== false) {
                throw new Exception('Spam Detected');
            }
        }
    }
}