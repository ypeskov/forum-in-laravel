<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 30.08.17
 * Time: 20:54
 */

namespace App\Inspections;


class KeyHeldDown implements InspectionInterface
{
    public function detect($body)
    {
        if (preg_match('/(.)\\1{4,}/', $body)) {
            throw new \Exception('Spam Detected');
        }
    }
}