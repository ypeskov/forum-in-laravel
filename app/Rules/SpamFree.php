<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 02.09.17
 * Time: 16:20
 */

namespace App\Rules;


use App\Inspections\Spam;

class SpamFree
{
    public function passes($attribute, $value)
    {
        try {
            return !resolve(Spam::class)->detect($value);
        } catch (\Exception $e) {
            return false;
        }

    }
}