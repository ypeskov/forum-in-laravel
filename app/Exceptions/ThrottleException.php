<?php
/**
 * Created by PhpStorm.
 * User: Yuriy Peskov <yuriy.peskov@gmail.com>
 * Date: 03.09.17
 * Time: 10:09
 */

namespace App\Exceptions;


class ThrottleException extends \Exception
{
}