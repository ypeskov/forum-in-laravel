<?php

header('Content-Type: application/json');

$userNames = [
    'Peter',
    'Юра',
    'Max',
    'Maximilanus'
];

$_GET['q'];

$results = array_values(array_filter($userNames, function($name) {
    return strpos($name, $_GET['q']) === 0;
}));

echo json_encode($results);