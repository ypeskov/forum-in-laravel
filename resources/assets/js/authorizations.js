/**
 * Created by Yuriy.Peskov<yuriy.peskov@gmail.com> on 25.09.17.
 */

let user = window.App.user;

module.exports = {
    updateReply(reply) {
        return reply.user_id === user.id;
    }
};